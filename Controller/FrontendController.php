<?php

namespace ATM\SurveyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class FrontendController extends Controller{

    /**
     * @Route("/see/text/answers/{answerId}", name="atm_survey_see_text_answer")
     */
    public function seeTextAnswerAction($answerId){
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();

        $qb
            ->select('answer')
            ->addSelect('survey')
            ->addSelect('user')
            ->addSelect('question')
            ->addSelect('askedBy')
            ->addSelect('choice')
            ->from('ATMSurveyBundle:Answer','answer')
            ->join('answer.survey','survey')
            ->join('survey.questions','question')
            ->leftJoin('question.askedBy','askedBy')
            ->leftJoin('question.choices','choice')
            ->join('answer.user','user')
            ->where($qb->expr()->eq('answer.id',$answerId));

        $answer = $qb->getQuery()->getResult()[0];
        $survey = $answer->getSurvey();
        $currentDate = new \DateTime();
        $diff = $currentDate->diff($survey->getInitDate());

        if($diff->days < 0 && !$this->get('xlabs_mm_admin')->isMMAdmin()){
            return $this->redirect($this->get('router')->generate('members_surveys'));
        }

        return $this->render('ATMSurveyBundle:Frontend:answer_text.html.twig',array(
            'answer' => $answer,
            'userAnswers' => json_decode($answer->getAnswers(),true)
        ));
    }

}