<?php

namespace ATM\SurveyBundle\Controller;

use ATM\SurveyBundle\Event\InteractiveQuestionAnswered;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use ATM\SurveyBundle\Entity\Answer;
use ATM\SurveyBundle\Entity\InteractiveQuestion;
use ATM\SurveyBundle\Event\InteractiveQuestionsCreated;
use ATM\SurveyBundle\Event\SurveyCompleted;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use \DateTime;

class SurveyController extends Controller
{
    /**
     * @Route("/survey/for/users", name="atm_survey_for_users")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $configuration = $this->getParameter('atm_survey_config');

        $users = $em->getRepository($configuration['user'])->findAll();
        $surveys = $em->getRepository('ATMSurveyBundle:Survey')->findAll();


        return $this->render('ATMSurveyBundle:Frontend:index.html.twig',array(
            'users' => $users,
            'surveys' => $surveys
        ));
    }

    /**
     * @Route("/survey/users/{surveyId}/{userId}", name="atm_survey_user")
     */
    public function surveyUserAction($surveyId,$userId){
        $em = $this->getDoctrine()->getManager();
        $configuration = $this->getParameter('atm_survey_config');

        $qb = $em->createQueryBuilder();
        $qb
            ->select('answer')
            ->addSelect('survey')
            ->from('ATMSurveyBundle:Answer','answer')
            ->join('answer.survey','survey','WITH',$qb->expr()->eq('survey.id',$surveyId))
            ->join('answer.user','user','WITH',$qb->expr()->eq('user.id',$userId));

        $answer = $qb->getQuery()->getResult();
        if(!empty($answer) && $answer[0]->getAnswers() && !$this->get('xlabs_mm_admin')->isMMAdmin()){
            return $this->redirect($this->get('router')->generate($configuration['redirect_route_after_survey']));
        }

        $user = $em->getRepository($configuration['user'])->findOneById($userId);

        $qbSurvey = $em->createQueryBuilder();
        $qbSurvey
            ->select('partial s.{id,name,creation_date}')
            ->addSelect('partial q.{id,question,type}')
            ->addSelect('partial a.{id,username,title,usernameCanonical,avatar}')
            ->addSelect('partial c.{id,choice}')
            ->from('ATMSurveyBundle:Survey','s')
            ->leftJoin('s.questions','q')
            ->leftJoin('q.askedBy','a')
            ->leftJoin('q.choices','c')
            ->where($qbSurvey->expr()->eq('s.id',$surveyId))
            ->orderBy('q.id','ASC')
        ;

        $survey = $qbSurvey->getQuery()->getArrayResult();

        $qbAnswer = $em->createQueryBuilder();
        $qbAnswer
            ->select('partial a.{id,answers_json}')
            ->from('ATMSurveyBundle:Answer','a')
            ->join('a.survey','s','WITH',$qbAnswer->expr()->eq('s.id',$surveyId))
            ->join('a.user','u','WITH',$qbAnswer->expr()->eq('u.id',$userId));

        $answer = $qbAnswer->getQuery()->getArrayResult();
        if(isset($answer[0])){
            $userAnswers = json_decode($answer[0]['answers_json'],true);
        }else{
            $userAnswers = array();
        }

        return $this->render('ATMSurveyBundle:Frontend:survey.html.twig',array(
            'user' => $user,
            'survey' => $survey[0],
            'user_answers' => $userAnswers
        ));
    }

    /**
     * @Route("/survey/submit/{surveyId}/{userId}", name="atm_survey_user_submit")
     */
    public function surveySubmitAction($surveyId,$userId){
        $em = $this->getDoctrine()->getManager();
        $configuration = $this->getParameter('atm_survey_config');

        $survey = $em->getRepository('ATMSurveyBundle:Survey')->findOneById($surveyId);
        $user = $em->getRepository($configuration['user'])->findOneById($userId);

        $qbAnswer = $em->createQueryBuilder();
        $qbAnswer
            ->select('partial a.{id,answers_json}')
            ->from('ATMSurveyBundle:Answer','a')
            ->join('a.survey','s','WITH',$qbAnswer->expr()->eq('s.id',$surveyId))
            ->join('a.user','u','WITH',$qbAnswer->expr()->eq('u.id',$userId));

        $answers = $qbAnswer->getQuery()->getOneOrNullResult();

        $request = $this->get('request_stack')->getCurrentRequest();
        $formData = $request->request->all();

        if($answers){
            $answers->setAnswersJson(json_encode($formData));
        }else{
            $answers = new Answer();
            $answers->setAnswersJson(json_encode($formData));
            $answers->setSurvey($survey);
            $answers->setUser($user);
        }


        $em->persist($answers);
        $em->flush();

        $event = new SurveyCompleted($survey,$answers,$user);
        $this->get('event_dispatcher')->dispatch(SurveyCompleted::NAME, $event);

        $configuration = $this->getParameter('atm_survey_config');
        return $this->redirect($this->get('router')->generate($configuration['redirect_route_after_survey']));
    }

    /**
     * @Route("/survey/autosave/{surveyId}/{userId}/{questionId}/{answerText}", name="atm_survey_autosave", options={"expose"=true})
     */
    public function surveyAutoSaveAnswersAction($surveyId,$userId,$questionId,$answerText){
        $em = $this->getDoctrine()->getManager();
        $configuration = $this->getParameter('atm_survey_config');

        $qbAnswer = $em->createQueryBuilder();
        $qbAnswer
            ->select('partial a.{id,answers_json}')
            ->from('ATMSurveyBundle:Answer','a')
            ->join('a.survey','s','WITH',$qbAnswer->expr()->eq('s.id',$surveyId))
            ->join('a.user','u','WITH',$qbAnswer->expr()->eq('u.id',$userId));

        $answer = $qbAnswer->getQuery()->getOneOrNullResult();

        if(!is_null($this->get('request_stack')->getCurrentRequest()->get('choice'))){
            $answerText = json_decode($answerText,true);
            $userAnswer = array('id'=>$questionId , 'choices'=>$answerText);
        }else{
            $answerText = urldecode($answerText);
            $userAnswer = array('id'=>$questionId , 'text'=>$answerText);
        }

        if($answer){
            $arrAnswers = $answer->getAnswersJson();
            if(!empty($answerText)){
                $arrAnswers['answers']['answer'.$questionId] = $userAnswer;
            }else{
                unset($arrAnswers['answers']['answer'.$questionId]);
            }
            $answer->setAnswersJson(json_encode($arrAnswers));
        }else{
            $survey = $em->getRepository('ATMSurveyBundle:Survey')->findOneById($surveyId);
            $user = $em->getRepository($configuration['user'])->findOneById($userId);

            $arrAnswers = array();
            $arrAnswers['answers'] = array('answer'.$questionId => $userAnswer);
            $answer = new Answer();
            $answer->setAnswersJson(json_encode($arrAnswers));
            $answer->setSurvey($survey);
            $answer->setUser($user);
        }

        $em->persist($answer);
        $em->flush();

        return new Response('ok');
    }

    /**
     * @Route("/submit/interactive/questions", name="atm_survey_submit_interactive_questions")
     */
    public function createInteractiveQuestionsAction(){
        $request = $this->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $configuration = $this->getParameter('atm_survey_config');

        if($request->getMethod() == 'POST'){
            $modelId = $request->get('model_id');
            $model = $em->getRepository($configuration['user'])->findOneById($modelId);

            $questions = $request->get('questions');
            foreach($questions as $question){
                if(!empty($question)){
                    $interactiveQuestion = new InteractiveQuestion();
                    $interactiveQuestion->setQuestion($question);
                    $interactiveQuestion->setModel($model);
                    $interactiveQuestion->setAuthor($user);
                    $em->persist($interactiveQuestion);
                }
            }

            $em->flush();

            $event = new InteractiveQuestionsCreated($user,$model);
            $this->get('event_dispatcher')->dispatch(InteractiveQuestionsCreated::NAME, $event);

            $this->get('session')->getFlashBag()->add('interactive_questions_submitted','Thanks for your questions');
            return $this->redirect($this->get('router')->generate('members_model_profile_interactive_questions',array(
                'usernameCanonical' => $model->getUsernameCanonical()
            )));
        }

        return $this->redirect($this->get('router')->generate('members_user_profile',array(
            'usernameCanonical' => $user->getUsernameCanonical()
        )));
    }

    /**
     * @Route("/interactive/question/{questionId}", name="atm_survey_interactive_question_answer")
     */
    public function answerInteractiveQuestionAction($questionId){
        $request = $this->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $interactiveQuestion = $em->getRepository('ATMSurveyBundle:InteractiveQuestion')->findOneById($questionId);

        // Check question is approved
        if(!$interactiveQuestion->getQuestionReviewed())
        {
            throw new NotFoundHttpException('The requested question doesn´t exist.');
        }

        // Check model is the destinatary
        if($interactiveQuestion->getModel()->getId() != $user->getId())
        {
            throw new AccessDeniedException('You are not the destinatary of this question.');
        }

        if($interactiveQuestion->getAnswerReviewed() || !empty($interactiveQuestion->getAnswer())){
            $this->get('session')->getFlashBag()->add('interactive_questions_submitted','You already answered this question');
            return $this->redirect($this->get('router')->generate('members_model_profile',array(
                'usernameCanonical' => $user->getUsernameCanonical()
            )));
        }

        if($request->getMethod() == 'POST'){

            $answer = $request->get('taAnswer');

            $interactiveQuestion->setAnswer($answer);
            $interactiveQuestion->setAnswerDate(new DateTime());
            $em->persist($interactiveQuestion);
            $em->flush();

            $event = new InteractiveQuestionAnswered($interactiveQuestion);
            $this->get('event_dispatcher')->dispatch(InteractiveQuestionAnswered::NAME, $event);


            $this->get('session')->getFlashBag()->add('interactive_questions_submitted','Thank you for your answer. Once approved it will appear on your profile.');
            return $this->redirect($this->get('router')->generate('members_model_profile_interactive_questions',array(
                'usernameCanonical' => $user->getUsernameCanonical()
            )));
        }

        return $this->render('ATMSurveyBundle:Frontend:InteractiveQuestions/answer.html.twig',array(
            'interactiveQuestion' => $interactiveQuestion,
            'user' => $interactiveQuestion->getAuthor(),
            'model' => $interactiveQuestion->getModel()
        ));

    }
}
