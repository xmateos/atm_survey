<?php

namespace ATM\SurveyBundle\Repository;

use Doctrine\ORM\EntityRepository;
use ATM\SurveyBundle\Entity\InteractiveQuestion;
use Doctrine\DBAL\Cache\QueryCacheProfile;

class InteractiveQuestionRepository extends EntityRepository
{
    public function getInteractiveQuestionsById($aParams)
    {
        $default_params = array(
            'result_ids' => false,
            'sorting' => 'date'
        );
        $aParams = array_merge($default_params, $aParams);

        if(!$aParams['result_ids'] || empty($aParams['result_ids']))
        {
            return array();
        }

        $qb = $this->getEntityManager()->createQueryBuilder();
        $aIds_qb = $qb
            ->select('DISTINCT iq.id')
            ->from(InteractiveQuestion::class,'iq');
        if(is_array($aParams['result_ids']))
        {
            $aIds_qb->where($aIds_qb->expr()->in('iq.id', $aParams['result_ids']));
        } else {
            $aIds_qb->where($aIds_qb->expr()->eq('iq.id', $aParams['result_ids']));
        }
        $aIds = array();
        switch($aParams['sorting'])
        {
            case 'date':
                $aIds_qb->orderBy('iq.creation_date','DESC');
                $aIds = $aIds_qb->getQuery()->getArrayResult();
                $aIds = array_map(function($v){
                    return $v['id'];
                }, $aIds);
                break;
            case 'field':
                $aIds = $aParams['result_ids'];
                break;
        }

        $results = array();
        foreach($aIds as $interactive_question_id)
        {
            $results[] = $this->getInteractiveQuestionById($interactive_question_id);
        }
        return array_filter($results);
    }

    public function getInteractiveQuestionById($interactive_question_id)
    {
        $em = $this->getEntityManager();
        $cache = $em->getConfiguration()->getResultCacheImpl();

        $qb = $em->createQueryBuilder();
        $qb
            ->select('iq')
            ->addSelect('m')
            ->addSelect('a')
            ->from(InteractiveQuestion::class,'iq')
            ->join('iq.model','m')
            ->join('iq.author','a')
            ->where($qb->expr()->eq('iq.id', $interactive_question_id));
        $query = $qb->getQuery();

        // When multiple joins are found in query, and associations can becoma large (like tags, actors, etc), the hydration takes long to get the array resultset; besides caching the result, also cache hydration result and PROBLEM RESOLVED!
        $hydrationCacheProfile = new QueryCacheProfile(InteractiveQuestion::RESULT_CACHE_ITEM_TTL, InteractiveQuestion::RESULT_CACHE_ITEM_PREFIX.$interactive_question_id.'_hydration', $cache);

        $interactive_question = $query
            ->useQueryCache(true)
            ->setResultCacheLifetime(InteractiveQuestion::RESULT_CACHE_ITEM_TTL)
            ->setResultCacheId(InteractiveQuestion::RESULT_CACHE_ITEM_PREFIX.$interactive_question_id)
            ->setHydrationCacheProfile($hydrationCacheProfile)
            ->getArrayResult();
        if($interactive_question)
        {
            $interactive_question = $interactive_question[0];
            return $interactive_question;
        } else {
            $em->getConfiguration()->getResultCacheImpl()->delete(InteractiveQuestion::RESULT_CACHE_ITEM_PREFIX.$interactive_question_id);
            $em->getConfiguration()->getResultCacheImpl()->delete(InteractiveQuestion::RESULT_CACHE_ITEM_PREFIX.$interactive_question_id.'_hydration');
            return false;
        }
    }
}