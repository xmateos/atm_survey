<?php

namespace ATM\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use XLabs\ResultCacheBundle\Annotations as XLabsResultCache;

/**
 * @ORM\Entity(repositoryClass="ATM\SurveyBundle\Repository\InteractiveQuestionRepository")
 * @ORM\Table(name="atm_interactive_question")
 * @XLabsResultCache\Clear(onFlush={}, {
 *      @XLabsResultCache\Key(onFlush={"update", "delete"}, type="literal", method="getXLabsResultCacheKeyForItem"),
 *      @XLabsResultCache\Key(onFlush={"insert", "update", "delete"}, type="prefix", method="getXLabsResultCacheKeyForCollection")
 * })
 */
class InteractiveQuestion
{
    // Keys used in PostRepository
    const RESULT_CACHE_ITEM_PREFIX = 'atm_interactive_question_';
    const RESULT_CACHE_ITEM_TTL = 604800; // 1 week
    // Keys used in SearchPost
    const RESULT_CACHE_COLLECTION_PREFIX = 'atm_interactive_questions_';
    const RESULT_CACHE_COLLECTION_TTL = 3600;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creation_date;

    /**
     * @ORM\Column(name="answer_date", type="datetime", nullable=true)
     */
    private $answer_date;

    /**
     * @ORM\Column(name="question", type="text", nullable=false, options={"collation": "utf8mb4_unicode_ci"})
     */
    private $question;

    /**
     * @ORM\Column(name="question_reviewed", type="boolean", nullable=false,  options={"default" : 0})
     */
    private $question_reviewed;

    /**
     * @ORM\Column(name="answer", type="text", nullable=true, options={"collation": "utf8mb4_unicode_ci"})
     */
    private $answer;

    /**
     * @ORM\Column(name="answer_reviewed", type="boolean", nullable=false,  options={"default" : 0})
     */
    private $answer_reviewed;

    protected $author;

    protected $model;

    public function __construct()
    {
        $this->creation_date = new \DateTime();
        $this->question_reviewed = false;
        $this->answer_reviewed = false;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    public function getAnswerDate()
    {
        return $this->answer_date;
    }

    public function setAnswerDate($answer_date)
    {
        $this->answer_date = $answer_date;
    }

    public function getQuestion()
    {
        return $this->question;
    }

    public function setQuestion($question)
    {
        $this->question = $question;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;
    }

    public function getQuestionReviewed()
    {
        return $this->question_reviewed;
    }

    public function setQuestionReviewed($question_reviewed)
    {
        $this->question_reviewed = $question_reviewed;
    }

    public function getAnswerReviewed()
    {
        return $this->answer_reviewed;
    }

    public function setAnswerReviewed($answer_reviewed)
    {
        $this->answer_reviewed = $answer_reviewed;
    }

    public function getAnswer()
    {
        return $this->answer;
    }

    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }

    public function getXLabsResultCacheKeyForItem()
    {
        // UserRepository has totals; clean owner cache aswell
        $keys = array(
            $this::RESULT_CACHE_ITEM_PREFIX.$this->getId(),
            $this::RESULT_CACHE_ITEM_PREFIX.$this->getId().'_hydration',
        );
        return $keys;
    }

    public function getXLabsResultCacheKeyForCollection()
    {
        return $this::RESULT_CACHE_COLLECTION_PREFIX;
    }
}