<?php

namespace ATM\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="atm_answer")
 */
class Answer{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Survey", inversedBy="answers")
     */
    protected $survey;

    /**
     * @ORM\Column(name="answers_json", type="text",  nullable=true)
     */
    private $answers_json;

    /**
     * @ORM\Column(name="answers", type="text", nullable=true)
     */
    private $answers;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creation_date;

    protected $user;

    public function __construct()
    {
        $this->creation_date = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSurvey()
    {
        return $this->survey;
    }

    public function setSurvey($survey)
    {
        $this->survey = $survey;
    }

    public function getAnswersJson()
    {
        return json_decode($this->answers_json,true);
    }

    public function setAnswersJson($answers_json)
    {
        $this->answers_json = $answers_json;
    }

    public function getAnswers()
    {
        return $this->answers;
    }

    public function setAnswers($answers)
    {
        $this->answers = $answers;
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }
}