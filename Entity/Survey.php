<?php

namespace ATM\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="atm_survey")
 */
class Survey{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creation_date;

    /**
     * @ORM\OneToMany(targetEntity="Question", mappedBy="survey", cascade={"remove"})
     */
    protected $questions;

    /**
     * @ORM\Column(name="header_image", type="string", length=255, nullable=true)
     */
    private $header_image;

    /**
     * @ORM\Column(name="header_image_mobile", type="string", length=255, nullable=true)
     */
    private $header_image_mobile;

    /**
     * @ORM\Column(name="header_image_middle", type="string", length=255, nullable=true)
     */
    private $header_image_middle;

    /**
     * @ORM\Column(name="profile_image", type="string", length=255, nullable=true)
     */
    private $profile_image;

    /**
     * @ORM\Column(name="init_date", type="datetime", nullable=true)
     */
    private $init_date;

    /**
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     */
    private $end_date;

    /**
     * @ORM\OneToMany(targetEntity="Answer", mappedBy="survey")
     */
    protected $answers;

    public function __construct()
    {
        $this->creation_date = new \DateTime();
        $this->questions = new ArrayCollection();
        $this->answers = new ArrayCollection();
        $this->init_date = new \DateTime();
        $this->end_date = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    public function getQuestions()
    {
        return $this->questions;
    }

    public function addQuestion($question)
    {
        $this->questions->add($question);
    }

    public function removeQuestion($question){
        $this->questions->removeElement($question);
    }

    public function getHeaderImage()
    {
        return $this->header_image;
    }

    public function setHeaderImage($header_image)
    {
        $this->header_image = $header_image;
    }

    public function getProfileImage()
    {
        return $this->profile_image;
    }

    public function setProfileImage($profile_image)
    {
        $this->profile_image = $profile_image;
    }

    public function getInitDate()
    {
        return $this->init_date;
    }

    public function setInitDate($init_date)
    {
        $this->init_date = $init_date;
    }

    public function getEndDate()
    {
        return $this->end_date;
    }

    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
    }

    public function getAnswers()
    {
        return $this->answers;
    }

    public function getHeaderImageMobile()
    {
        return $this->header_image_mobile;
    }

    public function setHeaderImageMobile($header_image_mobile)
    {
        $this->header_image_mobile = $header_image_mobile;
    }

    public function getHeaderImageMiddle()
    {
        return $this->header_image_middle;
    }

    public function setHeaderImageMiddle($header_image_middle)
    {
        $this->header_image_middle = $header_image_middle;
    }
}