<?php

namespace ATM\SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="atm_question")
 */
class Question
{
    const type_text = 'text';
    const type_multiple_choice = 'multiple_choice';
    const single_choice = 'single_choice';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creation_date;

    /**
     * @ORM\Column(name="question", type="text", nullable=false)
     */
    private $question;

    /**
     * @ORM\Column(name="type", type="text", nullable=false)
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="Survey", inversedBy="questions" )
     */
    protected $survey;

    /**
     * @ORM\OneToMany(targetEntity="Choice", mappedBy="question", cascade={"remove","persist"})
     */
    protected $choices;

    protected $askedBy;

    public function __construct()
    {
        $this->creation_date = new \DateTime();
        $this->choices = new ArrayCollection();
        $this->type = self::type_text;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreationDate()
    {
        return $this->creation_date;
    }

    public function setCreationDate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    public function getQuestion()
    {
        return $this->question;
    }
    public function setQuestion($question)
    {
        $this->question = $question;
    }

    public function setSurvey($survey)
    {
        $this->survey = $survey;
    }

    public function getSurvey()
    {
        return $this->survey;
    }

    public function getChoices()
    {
        return $this->choices;
    }

    public function addChoice($choice){
        $this->choices->add($choice);
    }

    public function removeChoice($choice){
        $this->choices->removeElement($choice);
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getAskedBy()
    {
        return $this->askedBy;
    }

    public function setAskedBy($askedBy)
    {
        $this->askedBy = $askedBy;
    }
}