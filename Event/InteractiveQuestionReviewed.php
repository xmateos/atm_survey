<?php

namespace ATM\SurveyBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class InteractiveQuestionReviewed extends Event{

    const NAME = 'atm_interactive_question_reviewed.event';

    private $interactiveQuestion;


    public function __construct($interactiveQuestion)
    {
        $this->interactiveQuestion = $interactiveQuestion;
    }

    public function getInteractiveQuestion()
    {
        return $this->interactiveQuestion;
    }
}