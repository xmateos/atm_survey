<?php

namespace ATM\SurveyBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class InteractiveQuestionsCreated extends Event{

    const NAME = 'atm_interactive_questions_created.event';

    private $user;
    private $model;


    public function __construct($user,$model)
    {
        $this->user = $user;
        $this->model = $model;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getModel()
    {
        return $this->model;
    }
}