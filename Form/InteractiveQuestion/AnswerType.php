<?php

namespace ATM\SurveyBundle\Form\InteractiveQuestion;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use ATM\SurveyBundle\Entity\InteractiveQuestion;

class AnswerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('answer', TextareaType::class, array(
                'required' => true,
                'label' => '',
                'attr' => array(
                    'autocomplete' => 'off'
                )
            ))
            ->add('save', SubmitType::class, array(
                'attr' => array(
                    'class' => 'button _form_but _form_but_save',
                    'disabled' => false
                ),
                'label' => 'Save'
            ))
            ->add('cancel', ButtonType::class, array(
                'attr' => array(
                    'class' => 'button red _form_but _form_but_cancel'
                ),
                'label' => 'Cancel'
            ))
        ;
    }

    public function getBlockPrefix()
    {
        return 'atm_surveybundle_interactivequestiontype_answer';
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => InteractiveQuestion::class,
        ));
    }
}
